#!/bin/bash
#CONFIGURACION DE CONTAFUEGOS (iptables)
sudo pacman -S iptable 
sudo pacman -S iproute2 
sudo cp /etc/iptables/empty.rules /etc/iptables/iptables.rules
sudo systemctl enable iptables.service
sudo systemctl enable ip6tables.service
sudo systemctl start iptables.service
sudo systemctl start ip6tables.service
#Mas informacion
#https://wiki.archlinux.org/title/Iptables_(Espa%C3%B1ol)

#ENCRIPTA LOS DNS
sudo pacman -S dnscrypt-proxy
sudo systemctl enable dnscrypt-proxy.service
sudo systemctl start dnscrypt-proxy.service
sudo systemctl status dnscrypt-proxy.service

#BUSCAR ROOTKITS 
sudo pacman -Ss rkhunter
sudo rkhunter -c

#Antivirus clamav (correo/archivos)
sudo pacman -S clamav
sudo freshclam #actualizacion de firmas de virus
sudo systemctl enable clamav-daemon.service 
sudo systemctl start clamav-daemon.service #inicia demonio de actualizaciones 
sudo systemctl enable clamav-daemon.service
sudo systemctl start clamav-daemon.service #inicia demonio de analicis de datos
#Mas informacion https://wiki.archlinux.org/title/ClamAV_(Espa%C3%B1ol)

#Limpieza de archivos y basura 
sudo pacman -S bleachbit-git

#USB
	sudo pacman -S usbguard

