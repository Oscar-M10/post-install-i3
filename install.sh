#!/bin/bash
#script for installations of i3
sudo pacman -Sy && sudo pacman -Syu
#sudo pacman -S xorg i3 lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings wget --noconfirm
#sudo systemctl enable lightdm

#instalando aplicaciones de wifi
#sudo pacman -S networkmanager linux-firmware dhcpcd dhclient network-manager-applet --noconfirm
#sudo systemctl enable dhcpcd@eth0
#sudo systemctl start dhcpcd@eth0
#sudo systemctl enable NetworkManager --now

#una nueva actualizaciòn completa
#sudo pacman -Sy && sudo pacman -Syu 

#instalando configuraciòn de audio
#sudo pacman -S pulseaudio pavucontrol 

#instalando ahorro bateria LAPTOP
#sudo pacman -S tlp tlp-rdw base-devel --noconfirm
#systemctl enable tlp.service
#systemctl enable NetworkManager-dispatcher.service
#systemctl mask systemd-rfkill.service systemd-rfkill.socket
#pacman -S acpi_call

#instalacion de yay y snap
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ..
sudo rm -rf yay
#Enable snapd
git clone https://aur.archlinux.org/snapd.git
cd snapd
makepkg -si --noconfirm
cd ..
sudo rm -rf snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap

#kernel linux zen
#sudo -S linux-zen linux-zen-headers --noconfirm

#Instalación de programas importantes de i3 manipulación de archivos y menu´s
sudo pacman -S  zsh dmenu rofi moc neovim ranger nautilus alacritty firefox feh picom brightnessctl mpv dunst arandr lxappearance gnome-screenshot --noconfirm
sudo pacman -S python-pip python2 nodejs npm ruby rubygems unzip --noconfirm
pip install neovim dbus-python
gem install neovim
sudo npm i -g neovim
sudo pacman -S xsel fzf ripgrep fd the_silver_searcher prettier  wget jre8-openjdk-headless jdk-openjdk --nonconfirm

#look thema, iconos
sudo pacman -S xorg-fonts-misc ttf-font-awesome ttf-font-awesome-5 arc-gtk-theme arc-icon-theme numlockx papirus-icon-theme blueman neofetch --noconfirm
#https://fontawesome.com/cheatsheet
#Install oh-my-zsh via wget
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
#Extra adornos 
sudo pacman -S lsb bat 

#Hacer que arch reconozca dispositivos android y discos en formato NTFS 
sudo pacman -S mtpfs gvfs-mtp gvfs-gphoto2 ntfs-3g --noconfirm
#CREAR DIRECTORIOS
sudo pacman -S xdg-user-dirs
#sudo nvim /etc/xdg/user-dirs.defaults
xdg-user-dirs-update

#Instalacion de los virt-manager y paquetes necesarios para virtualizar
#sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
#sudo pacman -S ebtables iptables
#sudo pacman -S libguestfs
#sudo systemctl enable libvirtd.service
#sudo systemctl start libvirtd.service
#sudo systemctl status libvirtd.service
#sudo usermod -a -G libvirt $(whoami)
#sudo systemctl restart libvirtd.service

#Instalar repositorio de blackarch
curl -O https://blackarch.org/strap.sh
echo 5ea40d49ecd14c2e024deecf90605426db97ea0c strap.sh | sha1sum -c
chmod +x strap.sh
sudo ./strap.sh
sudo pacman -Syu
solo por curioso
